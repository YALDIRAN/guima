import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('about', { path: '/about' });
  this.route('works', { path: '/works' });
  this.route('contact', { path: '/contact' });
  this.route('home', { path: '/home' });
});

export default Router;
